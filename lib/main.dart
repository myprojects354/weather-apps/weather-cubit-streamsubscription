import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:weather_cubit_streamsubscription/cubits/temp_settings/temp_settings_cubit.dart';
import 'package:weather_cubit_streamsubscription/cubits/theme/theme_cubit.dart';
import 'package:weather_cubit_streamsubscription/cubits/weather/weather_cubit.dart';
import 'package:weather_cubit_streamsubscription/repositories/weather_repository.dart';
import 'package:weather_cubit_streamsubscription/services/weather_api_services.dart';
import 'package:http/http.dart' as http;

import 'pages/home_page.dart';

void main() async {
  await dotenv.load(fileName: '.env');
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => WeatherRepository(
        weatherApiServices: WeatherApiServices(
          httpClient: http.Client(),
        ),
      ),
      child: MultiBlocProvider(
        providers: [
          BlocProvider<WeatherCubit>(
            create: (context) => WeatherCubit(
              weatherRepository: context.read<WeatherRepository>(),
            ),
          ),
          BlocProvider<TempSettingsCubit>(
            create: (context) => TempSettingsCubit(),
          ),
          BlocProvider<ThemeCubit>(
            create: (context) => ThemeCubit(
              weatherCubit: context.read<WeatherCubit>(),
            ),
          ),
        ],
        child: BlocBuilder<ThemeCubit, ThemeState>(
          builder: (context, state) {
            return MaterialApp(
              title: 'Weather App',
              debugShowCheckedModeBanner: false,
              theme: state.appTheme == AppTheme.light
                  ? //ThemeData.light().copyWith(primaryColorLight: Colors.green)
                  ThemeData.light().copyWith(
                      colorScheme:
                          const ColorScheme.light(primary: Colors.green))
                  : ThemeData.dark().copyWith(
                      colorScheme:
                          const ColorScheme.dark(primary: Colors.green)),
              home: const HomePage(),
            );
          },
        ),
      ),
    );
  }
}
